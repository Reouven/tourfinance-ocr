package com.fractalite.ocr;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.pdfbox.text.TextPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import technology.tabula.Ruling;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomPDFTextAreaStripper extends PDFTextStripper {

    private final List<String> regions = new ArrayList<>();
    private final Map<String, Rectangle2D> regionArea = new HashMap<>();
    private final Map<String, ArrayList<List<TextPosition>>> regionCharacterList
            = new HashMap<>();
    private final Map<String, StringWriter> regionText = new HashMap<>();
    private static final Logger LOG = LoggerFactory.getLogger(CustomPDFTextAreaStripper.class);


    public CustomPDFTextAreaStripper() throws IOException
    {
        super.setShouldSeparateByBeads(false);
    }

    public void extractRegions( PDPage page ) throws IOException
    {
        for (String regionName : regions)
        {
            setStartPage(getCurrentPageNo());
            setEndPage(getCurrentPageNo());
            // reset the stored text for the region so this class can be reused.
            ArrayList<List<TextPosition>> regionCharactersByArticle = new ArrayList<>();
            regionCharactersByArticle.add(new ArrayList<>());
      //      LOG.info("**********regionName   ",regionName,"   ***regionCharactersByArticle ",regionCharactersByArticle);

            regionCharacterList.put( regionName, regionCharactersByArticle );
            regionText.put( regionName, new StringWriter() );
        }

        if( page.hasContents() )
        {
            processPage( page );
        }
    }

    public void addRegion( String regionName, Rectangle2D rect )
    {
        regions.add( regionName );
        regionArea.put( regionName, rect );
    }

    /**
     * Delete a region to group text by. If the region does not exist, this method does nothing.
     *
     * @param regionName The name of the region to delete.
     */
    public void removeRegion(String regionName)
    {
        regions.remove(regionName);
        regionArea.remove(regionName);
    }

    public List<String> getRegions()
    {
        return regions;
    }

    public String getTextForRegion( String regionName )
    {
        StringWriter text = regionText.get( regionName );
        return text.toString();
    }
    @Override
    protected void processTextPosition(TextPosition text)
    {
        regionArea.forEach((key, rect) ->
        {
            if (isTextBelongToRect(rect, text))
            {
                charactersByArticle = regionCharacterList.get(key);
                super.processTextPosition(text);
            }
        });
    }


    public static Point2D getIntersections(Line2D a, Line2D b) {
        double x1 = a.getX1(), y1 = a.getY1(), x2 = a.getX2(), y2 = a.getY2(), x3 = b.getX1(), y3 = b.getY1(),
                x4 = b.getX2(), y4 = b.getY2();
        double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (d == 0) {
            return null;
        }

        double xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
        double yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;

        return new Point2D.Double(xi, yi);
    }

    private boolean isTextBelongToRect(Rectangle2D rect, TextPosition text) {
        Point2D points[] = new Point2D[2];
        float h = text.getHeight();
        float w = text.getWidth();
        Ruling textLine = new Ruling(text.getY()-h,text.getX(), w, h);
        Ruling clippedLine = textLine.intersect(rect);
        if (clippedLine != textLine)
            return clippedLine.length() >= textLine.length() / 2;
        return false;
    }

    @Override
    protected void writePage() throws IOException
    {
        for (String region : regionArea.keySet())
        {
            charactersByArticle = regionCharacterList.get( region );
            output = regionText.get( region );
            super.writePage();
        }
    }
}
