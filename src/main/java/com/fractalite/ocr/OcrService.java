package com.fractalite.ocr;

import com.aspose.pdf.Document;
import com.aspose.pdf.MemoryExtender;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.spire.ms.System.Collections.ArrayList;
import org.apache.commons.io.input.CharSequenceInputStream;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import technology.tabula.Ruling;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import com.fractalite.ocr.MatrixManipulator.MatrixCell;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static com.fractalite.ocr.OcrUtils.generateUniqueFileName;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.highestOneBit;
import static java.lang.Integer.parseInt;

@Service
public class OcrService {

    final Path workingDir = Paths.get(System.getProperty("user.dir"));
    final String dir = "generated_cords";
    final  Path generatedCordPath = workingDir.resolve(dir);

    static  int rectCount = 0;

    final int tolerance = 3;
    Map<String, List<OcrController.RectsInfo>> rectanglesInfo = new HashMap<String, List<OcrController.RectsInfo>>();

    private static final Logger LOG = LoggerFactory.getLogger(OcrService.class);
    private boolean corrected;

    public  OcrService() {
        loadCords();
        MemoryExtender.setSkipHeavyContentEnabled(true);
        MemoryExtender.isOptimizedMemoryStreamByDefault(true);
    }

    public void loadCords() {
        LOG.info("working dir is {}", workingDir);
        LOG.info("generated_cords dir is {}", generatedCordPath);
        try {
            Files.createDirectories(generatedCordPath);
        } catch (IOException e) {
            LOG.warn("Can't create Folder {} -> {}.\r\n This will lead to not save new coordinates", generatedCordPath, e.getMessage());
        }
        File file = new File(generatedCordPath.toString());
        List<File> listFiles = Arrays.asList(file.listFiles());
        for (final File fileEntry : listFiles) {
            if (fileEntry.isFile()) {
                OcrController.RectsInfo rect = new OcrController.RectsInfo();
                rect.filename = fileEntry.getName();
                try  {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileEntry)));
                    CSVReader csvReader = new CSVReader(reader);
                    csvReader.skip(1);
                    Iterator<String[]> it = csvReader.iterator();
                    while (it.hasNext()) {
                        String[] fields = it.next();
                        if (fields.length == 7) {
                            rect.hash = hashRulings(fields[6]);
                            rect.firstPageLines = fields[6];
                        }
                        if (fields.length >= 5) {
                            rect.rectangles.add(new OcrController.FieldInfo("",
                                    parseInt(fields[0]),
                                    parseFloat(fields[1]),
                                    parseFloat(fields[2]),
                                    parseFloat(fields[3]),
                                    parseFloat(fields[4]),
                                    false, ""));
                        }
                    }
                    addRectangle(rect);
                    csvReader.close();
                } catch (Exception e) {
                    LOG.info("Exception", e.getMessage());
                }
            }
        }
    }

    public void addRectangle(OcrController.RectsInfo rect) {
        if (rectanglesInfo.containsKey(rect.hash))
            rectanglesInfo.get(rect.hash).add(rect);
        else
            rectanglesInfo.put(rect.hash, new LinkedList<OcrController.RectsInfo>(Arrays.asList(rect)));
    }

    public boolean saveRectangle(OcrController.RectsInfo rect, String filename) {
        // save to a file
        try {
            filename = filename.replaceAll(" +", "_") + "_" + rect.hash.replaceAll("[/\\ ]", ".");

            ClassLoader classLoader = this.getClass().getClassLoader();
            File file = new File(generatedCordPath.resolve(filename).toString());

            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
            CSVWriter writer = new CSVWriter(osw);
            writer.writeNext(new String[]{"page", "x", "y", "width", "height", "hash", "firstPageLines"});
            for (OcrController.FieldInfo info : rect.rectangles) {
                if (rect.rectangles.indexOf(info) == 0)
                    writer.writeNext(new String[]{info.page+"", info.x+"", info.y+"", info.width+"", info.height+"", rect.hash, rect.firstPageLines});
                else
                    writer.writeNext(new String[]{info.page+"", info.x+"", info.y+"", info.width+"", info.height+""});
            }
            writer.close();
        } catch (Exception e) {
            LOG.warn("cord file not saved: {}", e.getMessage());
            return false;
        } finally {
            addRectangle(rect);
        }
        LOG.info("cord file saved successfully! {}", filename);
        return true;
    }

    public OcrController.RectsInfo findOrExtractRectangles(PDDocument doc, String filename) {
        rectCount = 0;
        List<List<Ruling>[]> pagesRulings = getPagesRulings(doc, filename);
//        String combinedRulings = RulingsTOString(getPageRulings(doc.getPage(1)));
        String combinedRulings = RulingsTOString(pagesRulings);
        String hash = hashRulings(combinedRulings);
        if (rectanglesInfo.containsKey(hash)) {
            for (OcrController.RectsInfo rect : rectanglesInfo.get(hash)) {
                if (rect.firstPageLines.equals(combinedRulings));
                    LOG.info("found rectangle info with hash: {} {}", rect.hash, rect.filename);
                    return rect;
            }
        }
        LOG.info("Cannot find any exiting rect info for this file extract them and save");
        OcrController.RectsInfo newRect = extractRectanglesFromPdf(doc, pagesRulings);
        newRect.firstPageLines = combinedRulings;
        newRect.hash = hashRulings(combinedRulings);
        saveRectangle(newRect, filename);
        return newRect;
    }

    public OcrController.RectsInfo extractRectanglesFromPdf(PDDocument doc, List<List<Ruling>[]> pagesRulings) {
        // return RectsInfo list or null
        // find a rectangle by converting a pdf first page to svg then compare it's lines to the registred one.

        OcrController.RectsInfo rect = new OcrController.RectsInfo();

        int pageCount = doc.getPages().getCount();
        for (int page_idx = 1 ; page_idx <= pageCount; page_idx++) {
            PDPage pg = doc.getPages().get(page_idx - 1);
            float height = OcrUtils.getPDPageHeight(pg);
            float width = OcrUtils.getPDPageWidth(pg);
            LOG.info("document size {} {} {}", page_idx, width, height);
            List<Ruling>[] lines = pagesRulings.get(page_idx - 1);
            try {
                drawRects(doc, lines[0],page_idx);
                drawRects(doc, lines[1],page_idx);
            }catch (Exception e){}
            if (lines != null) {
                LOG.info("page {} rulings size is {}:h  {}:v", page_idx, lines[0].size(), lines[1].size());
                Map<Point2D, Ruling[]> intersections = SvgLinesDetector.findIntersections(lines[0], lines[1]);

                MatrixCell[][] matInters = generateIntersectionsMatrix(intersections, width,height);
                rect.rectangles.addAll(correctFieldsCord(getFieldsCord(matInters, width, height, page_idx)));
                if (page_idx == 1)
                {
                    rect.firstPageLines = lines.toString();
                    rect.hash = hashRulings(rect.firstPageLines);
                }
            }
            else
                LOG.info("no rulings for page {}", page_idx);
        }
        return rect;
    }

    List<Ruling>[] getPageRulings(PDPage page,String filename,AtomicInteger i) {
        com.aspose.pdf.SvgSaveOptions saveOptions = new com.aspose.pdf.SvgSaveOptions();
        saveOptions.setCompressOutputToZipArchive(false);
        // convert page to svg
        com.aspose.pdf.Document pdfDocument = new com.aspose.pdf.Document(OcrUtils.getPageBytes(page));
        StringWriter writer = new StringWriter();
        OutputStream ops = new WriterOutputStream(writer, StandardCharsets.UTF_8);
        pdfDocument.save(ops, saveOptions);
        try {
            ops.flush();
        } catch (Exception e) {}

        // detect line from the svg
        SvgLinesDetector linesDetector = new SvgLinesDetector();
        if (linesDetector.setDoc(new CharSequenceInputStream(writer.getBuffer(), StandardCharsets.UTF_8))) {
           return linesDetector.detect(OcrUtils.getPDPageWidth(page), OcrUtils.getPDPageHeight(page), filename,i);
        }
        return null;
    }

    List<List<Ruling>[]> getPagesRulings(PDDocument doc,String filename) {
        List<List<Ruling>[]> pagesRulings = new ArrayList(doc.getPages().getCount());
        List<PDPage> pages= new LinkedList<PDPage>();
        doc.getPages().iterator().forEachRemaining(pages::add);
        for (int i= 0; i < doc.getPages().getCount(); i++)
            pagesRulings.add(null);
        AtomicInteger i = new AtomicInteger(0);

        pages.stream().forEach(p ->
        {
            pagesRulings.set(doc.getPages().indexOf(p),getPageRulings(p, filename,i));
            i.getAndIncrement();
        });
        return pagesRulings;
    }

    private String hashRulings(String rulings) {
        String hash = "";
        try {
            byte[] bytesOfMessage = rulings.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] theMD5digest = md.digest(bytesOfMessage);
            hash = Base64.getEncoder().encodeToString(theMD5digest);
            LOG.info("hashRulings {}", hash);
            return hash;
        } catch (NoSuchAlgorithmException e) {
            LOG.info("No MD5 to hash rulings");
        } catch (UnsupportedEncodingException e) {
            LOG.info("Cannot get bytes from string: UnsupportedEncodingException");
        }
        return hash;
    }

    String RulingsTOString(List<Ruling>[] rulings) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 2; i++)
            for (Ruling r : rulings[i]) {
                sb.append(OcrUtils.fromBase10ToBaseN((int)r.getP1().getX(), 127))
                  .append(OcrUtils.fromBase10ToBaseN((int)r.getP1().getY(), 127))
                  .append(OcrUtils.fromBase10ToBaseN((int)r.getP2().getX(), 127))
                  .append(OcrUtils.fromBase10ToBaseN((int)r.getP2().getY(), 127));
            }
        return sb.toString();
    }

    String RulingsTOString(List<List<Ruling>[]> pagesRulings) {
        StringBuilder sb = new StringBuilder();
        for (List<Ruling>[] rulings: pagesRulings)
            for (int i = 0; i < 2; i++)
                for (Ruling r : rulings[i]) {
                    sb.append(OcrUtils.fromBase10ToBaseN((int)r.getP1().getX(), 127))
                            .append(OcrUtils.fromBase10ToBaseN((int)r.getP1().getY(), 127))
                            .append(OcrUtils.fromBase10ToBaseN((int)r.getP2().getX(), 127))
                            .append(OcrUtils.fromBase10ToBaseN((int)r.getP2().getY(), 127));
            }
        return sb.toString();
    }

    MatrixCell[][] generateIntersectionsMatrix(Map<Point2D, Ruling[]> intersections, double width, double height) {
        MatrixCell[][] mat;

        mat = new MatrixCell[(int) Math.round(height) + 1][(int) Math.round(width) + 1];
        for (Point2D p: intersections.keySet()) {
            if (mat[(int) Math.round(p.getY())][(int) Math.round(p.getX())] != null) {
                MatrixCell oldCell = mat[(int) Math.round(p.getY())][(int) Math.round(p.getX())];
                oldCell.VRuling.setTop(Math.min(oldCell.VRuling.getTop(), intersections.get(p)[1].getTop()));
                oldCell.VRuling.setBottom(Math.max(oldCell.VRuling.getBottom(), intersections.get(p)[1].getBottom()));
                oldCell.HRuling.setLeft(Math.min(oldCell.HRuling.getLeft(), intersections.get(p)[0].getLeft()));
                oldCell.HRuling.setRight(Math.max(oldCell.HRuling.getRight(), intersections.get(p)[0].getRight()));
            } else {
                MatrixCell cell = new MatrixCell(p, intersections.get(p)[0], intersections.get(p)[1]);
                mat[(int) Math.round(p.getY())][(int) Math.round(p.getX())] = cell;
            }
        }
        return mat;
    }

    List<OcrController.FieldInfo> getFieldsCord(MatrixCell[][] inters, double width, double height, int pageIdx)
    {
        List<OcrController.FieldInfo> fieldsInfo = new LinkedList<>();

        // some Magic happens here!!!
        for (int y = 0; y <= height; y++) {
            for (int x = 0; x <= width; x++) {
                if (inters[y][x] != null && inters[y][x].enable) {
                    int y1 = y;
                    double limit = inters[y][x].VRuling.getBottom();
                    while (y1 <= limit) {
                        MatrixCell pb = MatrixManipulator.getPointY(inters, x, y1 + 1, 0);
                        if (pb != null) {
                            y1 = (int) Math.round(pb.point.getY());
                            int x1 = x;
                            while (x < width) {
                                MatrixCell pr = MatrixManipulator.getPointX(inters, x1 + 1, y1, tolerance);
                                if (pr != null) {
                                    x1 = (int) Math.round(pr.point.getX());
                                    Ruling LRL = new Ruling(new Point2D.Double(pb.point.getX(), pb.point.getY()),new Point2D.Double(pr.point.getX(), pb.point.getY()));
                                    if (pr.VRuling.getTop() - 1 <= y &&
                                        !MatrixManipulator.isIntersected(MatrixManipulator.getPointsX(inters,x,y,x1, tolerance),LRL)) {
                                        double w = pr.point.getX() - inters[y][x].point.getX();
                                        double h = pr.point.getY() - inters[y][x].point.getY();
                                        String val;
                                        if (w > 2 && h > 2)
                                            fieldsInfo.add(new OcrController.FieldInfo("",
                                                                                        pageIdx,
                                                                                        inters[y][x].point.getX(),
                                                                                        inters[y][x].point.getY(),
                                                                                        w, h, false, ""));
                                        MatrixManipulator.cleanArea(inters, x + 1,y + 1, x1, y1);
                                        y1 = inters.length;
                                        x = x1 - 1;
                                        break;
                                    }
                                } else
                                    break;
                            }
                        } else
                            break;
                    }
                }
            }
        }
        return fieldsInfo;
    }

    List<OcrController.FieldInfo> correctFieldsCord(List<OcrController.FieldInfo> rects) {
        // loop over coord
        // for each one find the if there is any rect that should be after the current one
        // if found one move the current one before the founded one
//        rects = rects.stream().sorted((a,b) -> { return (int)(a.y - b.y);}).collect(Collectors.toList());
        if (rects == null)
                return null;
        LinkedList<OcrController.FieldInfo> finalRects = new LinkedList();
        finalRects.addAll(rects);
        ReentrantLock lock = new ReentrantLock();
        corrected = true;
        while (corrected) {
            corrected = false;
            rects.stream().forEach(rect -> {
                List<OcrController.FieldInfo> nearest = findNearestRects(rect, rects);
                if (nearest.size() > 0) {
                    this.corrected = true;
                    lock.lock();
                    int idx = finalRects.indexOf(nearest.get(0));
                    finalRects.remove(rect);
                    finalRects.add(idx, rect);
                    lock.unlock();
                }
            });
            if (corrected) {
                rects.clear();
                rects.addAll(finalRects);
            }
        }
        return finalRects;
    }

    List<OcrController.FieldInfo> findNearestRects(OcrController.FieldInfo rect, List<OcrController.FieldInfo> rects) {
        List<OcrController.FieldInfo> ret = new ArrayList();
        double miny = rect.y -  tolerance;
        int idx = rects.indexOf(rect) - 1;
        Point2D RTPoint = new Point2D.Double(rect.x + rect.width, rect.y);
        double minDistance = RTPoint.distance(RTPoint.getX() + tolerance,miny);
        while (idx >= 0 && rects.get(idx).y >= miny) {
            OcrController.FieldInfo tmpRect = rects.get(idx);
            if (tmpRect.startPoint.distance(RTPoint) <= minDistance && rect.y - tmpRect.y <= tmpRect.height / 2)
                ret.add(tmpRect);
            idx--;
        }
        ret.sort((a,b) -> {return (int) (a.startPoint.distance(RTPoint) - b.startPoint.distance(RTPoint));});
        return ret;
    }

    // Note: the DrawRects method is for debug purpose
    private void drawIntersections(PDDocument document, Map<Point2D, Ruling[]> points, int pageNumber) throws IOException {
        {
            for (Point2D point : points.keySet()) {
                PDPage page = document.getPage(pageNumber - 1);
                PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true);
                contentStream.setNonStrokingColor(Color.BLUE);
                contentStream.addRect((float) point.getX(), (float)(OcrUtils.getPDPageWidth(page) - (point.getY() + 2)), 2, 2);
//                contentStream.addRect((float) point.getX(), (float) point.getY(), 2, 2);
                contentStream.fill();
                contentStream.close();
            }
            saveDoc(document, "testIntersection");
        }
    }
    private void drawRects(PDDocument document, List<Ruling> rulings, int pageNumber) throws IOException {
        {
            LOG.info("phase 1.2");
            int i = 0;
            for (Ruling ruling : rulings) {
                double height = ruling.getHeight();
                double width = ruling.getWidth();
//                System.out.println(String.format("draw line at %f,%f size %f,%f", ruling.getX1(), ruling.getY1(), ruling.getWidth(), height));
                PDPage page = document.getPage(pageNumber - 1);
                PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true);
                contentStream.setNonStrokingColor(Color.BLUE);
                contentStream.addRect((float)ruling.getX1(), (float)(OcrUtils.getPDPageHeight(page) - (ruling.getY1() + height)), (float)Math.max(width, 1), (float)Math.max(height,1));
//                contentStream.addRect((float)ruling.getX1(), (float) ruling.getY1(), (float)Math.max(Math.abs(width), 1), (float)Math.max(Math.abs(height), 1));
                contentStream.fill();
                contentStream.close();
//                break;
            }
            saveDoc(document, "testIntersection");
        }
    }
    private void saveDoc(PDDocument document, String savePath) throws IOException {
        System.out.println("save fiel as: " + savePath);
        document.save(savePath);
    }
    public static void writeDocumentToFile(org.w3c.dom.Document doc, String fileName) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(fileName));
            transformer.transform(source, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
