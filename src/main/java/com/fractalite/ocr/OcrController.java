package com.fractalite.ocr;

import io.github.jonathanlink.PDFLayoutTextStripper;
import com.fractalite.ocr.MatrixManipulator.MatrixCell;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.PDFTextStripper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import org.apache.pdfbox.Loader;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.pdfbox.pdmodel.PDPage;
import javax.imageio.ImageIO;

import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.json.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVReader;
import technology.tabula.*;

import static com.fractalite.ocr.OcrUtils.generateUniqueFileName;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/ocr")
public class OcrController {

    @Autowired
    OcrService ocrService;

    private static final Logger LOG = LoggerFactory.getLogger(OcrController.class);
    private static List<FieldInfo> FieldsInfos = new LinkedList<FieldInfo>();
    private static List<FieldInfo> BN_FieldsInfos = new LinkedList<FieldInfo>();
    static class FieldInfo  {
        public String fieldName;
        public int page;
        public float x;
        public float y;
        Point2D startPoint;
        public float width;
        public float height;
        public String name;
        public String value;
        public boolean isNumber;
        FieldInfo(String fieldName, int page, int x, int y, int width, int height, boolean isNumber, String name) {
            this.fieldName = fieldName;
            this.page = page;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.isNumber = isNumber;
            this.name = name;
            startPoint = new Point2D.Double(x,y);
        }
        FieldInfo(String fieldName, int page, float x, float y, float width, float height, boolean isNumber, String name) {
            this.fieldName = fieldName;
            this.page = page;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.isNumber = isNumber;
            this.name = name;
            startPoint = new Point2D.Double(x,y);
        }
        FieldInfo(String fieldName, int page, double x, double y, double width, double height, boolean isNumber, String name) {
            this.fieldName = fieldName;
            this.page = page;
            this.x = (float) x;
            this.y = (float) y;
            this.width = (float) width;
            this.height = (float) height;
            this.isNumber = isNumber;
            this.name = name;
            startPoint = new Point2D.Double(x,y);
        }
    }

    static class RectsInfo  {
        List<FieldInfo> rectangles = new LinkedList<>();
        String hash;
        String filename;
        String firstPageLines;
    }

    @Value("${tesseract.path}")
    private String tesseractPath;

    @Value("${csv.normal.path}")
    private String csvNormalPath;

    @Value("${csv.simple.path}")
    private String csvSimplePath;


    @GetMapping("/ping")
    public ResponseEntity<String> get() {
        return new ResponseEntity<String>("PONG", HttpStatus.OK);
    }

    @PostMapping(path = "/pdf/bilan1/simple", produces = {"application/json;charset=UTF-8", "application/json"})
    public @ResponseBody ResponseEntity<String>  extractTextFromPDFFileBilanSimpleTest(@RequestParam("file") MultipartFile file) {
        try {
            PDDocument document = Loader.loadPDF(file.getBytes());
            PDFTextStripper reader = new PDFLayoutTextStripper();
            String strippedText = reader.getText(document);
            JSONObject obj = new JSONObject();
            obj.put("fileName", file.getOriginalFilename());
            obj.put("pdf", strippedText);
            return new ResponseEntity<>(obj.toString(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    private void drawRects(PDDocument document, Map<Point2D, Ruling[]> points, int pageNumber) throws IOException {
        {
            for (Point2D point : points.keySet()) {
                PDPage page = document.getPage(pageNumber - 1);
                PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true);
                contentStream.setNonStrokingColor(Color.BLUE);
//                contentStream.addRect((float) point.getX(), (float)(page.getMediaBox().getHeight() - (point.getY() + 4)), 4, 4);
                contentStream.addRect((float) point.getX(), (float) point.getY(), 4, 4);
                contentStream.fill();
                contentStream.close();
            }
            saveDoc(document, "testIntersection");
        }
    }

    private void drawRects(PDDocument document, ArrayList<Point2D> points, int pageNumber) throws IOException {
        {
            for (Point2D point : points) {
                PDPage page = document.getPage(pageNumber - 1);
                PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true);
                contentStream.setNonStrokingColor(Color.BLUE);
                contentStream.addRect((float) point.getX(), (float)(page.getMediaBox().getHeight() - (point.getY() + 4)), 4, 4);
//                contentStream.addRect((float) point.getX(), (float) point.getY(), 1, 1);
                contentStream.fill();
                contentStream.close();
            }
            saveDoc(document, "testIntersection");
        }
    }

    private void drawRects(PDDocument document, List<Ruling> rulings, int pageNumber) throws IOException {
        {
            LOG.info("phase 1.2");
            int i = 0;
            for (Ruling ruling : rulings) {
                double height = ruling.getHeight();
                double width = ruling.getWidth();

//                System.out.println(String.format("draw line at %f,%f size %f,%f", ruling.getX1(), ruling.getY1(), ruling.getWidth(), height));
                PDPage page = document.getPage(pageNumber - 1);
                PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true);
                contentStream.setNonStrokingColor(Color.BLUE);
                contentStream.addRect((float)ruling.getX1(), (float)(page.getMediaBox().getHeight() - (ruling.getY1() + height)), (float)Math.max(width, 1), (float)Math.max(height,1));
//                contentStream.addRect((float)ruling.getX1(), (float) ruling.getY1(), (float)Math.max(Math.abs(width), 0.5), (float)Math.max(Math.abs(height), 0.5));
                contentStream.fill();
                contentStream.close();
//                break;
            }
            saveDoc(document, "testIntersection");
        }
    }
    private void saveDoc(PDDocument document, String savePath) throws IOException {
        System.out.println("save fiel as: " +savePath);
        document.save(savePath);
    }

    @PostMapping(path = "/pdf/bilan/simple", produces = {"application/json;charset=UTF-8", "application/json"})
    public @ResponseBody ResponseEntity<String>  extractTextFromPDFFileBilanSimple(@RequestParam("file") MultipartFile file) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("fileName", file.getOriginalFilename());
            PDDocument document = Loader.loadPDF(file.getBytes());
            LOG.info("pages {}", document.getPages().getCount());
            if (document.getPages().getCount() == 3) {

                PDFTextStripper stripper = new PDFTextStripper();
                stripper.setSortByPosition(true);

                stripper.setStartPage(1);
                stripper.setEndPage(1);
                LOG.info("page 1 \n{}", stripper.getText(document));
                obj.put("page1", stripper.getText(document));

                stripper.setStartPage(2);
                stripper.setEndPage(2);
                LOG.info("page 2 \n{}", stripper.getText(document));
                obj.put("page2", stripper.getText(document));

                stripper.setStartPage(3);
                stripper.setEndPage(3);
                LOG.info("page 3 \n{}", stripper.getText(document));
                obj.put("page3", stripper.getText(document));

                PDPage page = document.getPages().get(1);
                LOG.info("MediaBox Height {}", page.getMediaBox().getHeight());
                LOG.info("MediaBox Width {}", page.getMediaBox().getWidth());
                LOG.info("LowerLeftX {}", page.getMediaBox().getLowerLeftX());
                LOG.info("LowerLeftY {}", page.getMediaBox().getLowerLeftY());
                LOG.info("UpperRightX {}", page.getMediaBox().getUpperRightX());
                LOG.info("UpperRightY {}", page.getMediaBox().getUpperRightY());

                initFieldInfo(csvSimplePath, FieldsInfos);
                for (FieldInfo fieldInfo : FieldsInfos) {
                    obj.put(fieldInfo.fieldName, extractARegion(document, fieldInfo));
                }
                return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
            }
            return new ResponseEntity<String>("PLease Provide the valid document", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/pdf/bilan", produces = {"application/json;charset=UTF-8", "application/json"})
    public @ResponseBody ResponseEntity<String>  extractTextFromPDFFileBilanSimpleGen2(@RequestParam("file") MultipartFile file) {
        try {
            List<Pair<String, FieldInfo>> pagesOutput = new LinkedList<>();
            CustomPDFTextAreaStripper stripper = new CustomPDFTextAreaStripper();
            JSONObject obj = new JSONObject();
            StringBuilder sb = new StringBuilder();
            PDFTextStripper stripper1 = new PDFTextStripper();
            stripper.setSortByPosition(true);


            PDDocument document = Loader.loadPDF(file.getBytes());
            //LOG.info("page iiii \n{}", stripper1.getText(document));

            LOG.info("file loaded as PDDocument");
            RectsInfo rectsInfo = ocrService.findOrExtractRectangles(document, file.getOriginalFilename());
            LOG.info("got rectangles info");
            rectsInfo.rectangles.forEach(p -> {pagesOutput.add(null);});

            List<OcrController.FieldInfo> rectangles = rectsInfo.rectangles;
            IntStream stream = IntStream.range(0, rectangles.size());
            LOG.info("prepared for extraction");
            // extract the content by area
            stream.forEach(i -> {
                FieldInfo info = rectangles.get(i);
                try {
                    String s = extractNextRegion(document, info.page, info.x, info.y, info.width, info.height);
          //         LOG.info("region contan.......////// ",s);
                    pagesOutput.set(i, new Pair<>(s, info));
                } catch (Exception e) {
                    LOG.error("Region extraction failed: {}", e.getMessage());
                    e.printStackTrace();
                }
            });

            // combined the content to one string for each page
            int lastPage = 1;
            for (Pair<String, FieldInfo> info : pagesOutput) {
                if (info != null) {
                    if (lastPage != info.getRight().page)
                    {
                        obj.put("page" + lastPage, sb.toString());
                        sb.delete(0, sb.length());
                    //    LOG.info("extraction info value//// ",sb.toString());


                        lastPage = info.getRight().page;
                    }
                    sb.append(info.getLeft()).append("|");
                }
            }
            LOG.info("extraction done");

            obj.put("page" + lastPage, sb.toString());
            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("stack trace {}", Arrays.toString(e.getStackTrace()));
            LOG.info("exception happened " + e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String extractNextRegion(PDDocument document, int pageNumber, double x, double y, double width, double height) throws Exception {
        CustomPDFTextAreaStripper stripper = new CustomPDFTextAreaStripper();
        stripper.setSortByPosition(true);
        Rectangle2D rect = new Rectangle2D.Double(x, y, width, height);
        stripper.addRegion("", rect);
        stripper.extractRegions(document.getPage(pageNumber - 1));
        //LOG.info("stripper .............///// ", "", stripper.getTextForRegion(""));
        String value = stripper.getTextForRegion("");
        value = value.trim().replaceAll(" +", " ");
        return value;
    }

    private Object extractARegion(PDDocument document, FieldInfo fieldInfo) throws Exception {
        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        stripper.setSortByPosition(true);
        Rectangle2D rect = new java.awt.geom.Rectangle2D.Float(fieldInfo.x, fieldInfo.y, fieldInfo.width, fieldInfo.height);
        stripper.addRegion(fieldInfo.name, rect);
        stripper.extractRegions(document.getPage(fieldInfo.page - 1));
        LOG.info("{}: {}", fieldInfo.name, stripper.getTextForRegion(fieldInfo.name));
        String value = stripper.getTextForRegion(fieldInfo.name);
//        if (value.contains(fieldInfo.name)) {
//        value = value.replaceFirst(fieldInfo.name, "");
        value = value.trim().replaceAll(" +", " ").trim();
        LOG.info("after trim {}", value);
        if (fieldInfo.isNumber) {
            int sign = 1;
            if (value.matches("\\(.*\\)"))
                sign = -1;
            value = value.replaceAll("\\D+","");
            if (value.isEmpty()) {
                return 0;
            }
            LOG.info("isNumber {}", value);
            return Float.valueOf(value) * sign;
        }
        if (value.isEmpty()) {
            return "";
        }
        return value;
//        }
//        throw new Exception("Could not find the region " + fieldInfo.name);
    }

    private Object extractARegion(PDDocument document, int pageNumber, int x, int y, int width, int height, String regionName, boolean isNumber) throws Exception {
        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        stripper.setSortByPosition(true);
        Rectangle2D rect = new java.awt.geom.Rectangle2D.Float(x, y, width, height);
        stripper.addRegion(regionName, rect);
        stripper.extractRegions(document.getPage(pageNumber - 1));
        LOG.info("{}: {}", regionName, stripper.getTextForRegion(regionName));
        String value = stripper.getTextForRegion(regionName);
        if(value.contains(regionName)) {
            value = value.replaceFirst(regionName, "");
            value = value.trim().replaceAll(" +", " ");
            LOG.info("after trim {}", value);
            if (isNumber) {
                if(value.isEmpty()) {
                    return 0;
                }
                value = value.replaceAll("\\D+","");
                LOG.info("isNumber {}", value);
                return Float.valueOf(value);
            }
            if(value.isEmpty()) {
                return "";
            }
            return value;
        }
        throw new Exception("Could not find the region " + regionName);
    }

    @PostMapping(path = "/pdf/bilan/normal", produces = {"application/json;charset=UTF-8", "application/json"})
    public @ResponseBody ResponseEntity<String>  extractTextFromPDFFileBilanNormal(@RequestParam("file") MultipartFile file) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("fileName", file.getOriginalFilename());
            PDDocument document = Loader.loadPDF(file.getBytes());
            LOG.info("pages {}", document.getPages().getCount());
            if (document.getPages().getCount() == 7) {

                PDFTextStripper stripper = new PDFTextStripper();
                stripper.setSortByPosition(true);

                for (int i = 1; i <= document.getPages().getCount() ; i++)
                {
                    stripper.setStartPage(i);
                    stripper.setEndPage(i);
                    LOG.info("page" + i + " \n{}", stripper.getText(document));
                    obj.put("page" + i, stripper.getText(document));
                }
                PDPage page = document.getPages().get(1);
                LOG.info("MediaBox Height {}", page.getMediaBox().getHeight());
                LOG.info("MediaBox Width {}", page.getMediaBox().getWidth());
                LOG.info("LowerLeftX {}", page.getMediaBox().getLowerLeftX());
                LOG.info("LowerLeftY {}", page.getMediaBox().getLowerLeftY());
                LOG.info("UpperRightX {}", page.getMediaBox().getUpperRightX());
                LOG.info("UpperRightY {}", page.getMediaBox().getUpperRightY());

                initFieldInfo(csvNormalPath, BN_FieldsInfos);
                for (FieldInfo fieldInfo : BN_FieldsInfos) {
                    obj.put(fieldInfo.fieldName, extractARegion(document, fieldInfo));
                }
//                document.save(new File("src/main/resources/Bilan_Normal_Rempli_debug.pdf"));
                document.close();
                return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
            }
            return new ResponseEntity<String>("PLease Provide the valid document", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<String>( "exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(path = "/pdf/bilan/complet", produces = {"application/json;charset=UTF-8", "application/json"})
    public @ResponseBody ResponseEntity<String>  extractTextFromPDFFileBilanComplet(@RequestParam("file") MultipartFile file) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("fileName", file.getOriginalFilename());
            PDDocument document = Loader.loadPDF(file.getBytes());
            PDFTextStripper reader = new PDFTextStripper();
            String strippedText = reader.getText(document);
            if (strippedText.trim().isEmpty()){
                strippedText = extractTextFromScannedDocument(document, 1);
            }
            obj.put("pdf", strippedText.toString());
            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/pdf/bilan/belge", produces = {"application/json;charset=UTF-8", "application/json"})
    public @ResponseBody ResponseEntity<String>  extractTextFromPDFFileBilanBelge(@RequestParam("file") MultipartFile file) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("fileName", file.getOriginalFilename());
            PDDocument document = Loader.loadPDF(file.getBytes());
            PDFTextStripper reader = new PDFTextStripper();
            String strippedText = reader.getText(document);
            if (strippedText.trim().isEmpty()){
                strippedText = extractTextFromScannedDocument(document, 1);
            }
            obj.put("pdf", strippedText.toString());
            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String extractTextFromScannedDocument(PDDocument document, int pageNumber) throws IOException, TesseractException {
        // Extract images from file
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        StringBuilder out = new StringBuilder();
        ITesseract _tesseract = new Tesseract();
        _tesseract.setDatapath(tesseractPath);
        _tesseract.setLanguage("fra");
        for (int page = 0; page < document.getNumberOfPages(); page++) {
            if (page == pageNumber) {
                BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                // Create a temp image file
                File temp = File.createTempFile("tempfile_" + page, ".png");
                ImageIO.write(bim, "png", temp);
                String result = _tesseract.doOCR(temp);
                out.append(result);
                // Delete temp file
                temp.delete();
            }
        }
        return out.toString();
    }

    private void initFieldInfo(String filepath, List<FieldInfo> Fields)
    {
        if (!Fields.isEmpty()) {
            return;
        }
//        Fields = new LinkedList<FieldInfo>();
        try (InputStream inputStream = getClass().getResourceAsStream(filepath);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            CSVReader csvReader = new CSVReader(reader);
            csvReader.skip(1);
            Iterator<String[]> it = csvReader.iterator();
            while (it.hasNext()) {
                String[] fields = it.next();
                if (fields.length == 7) {
                    Fields.add(new FieldInfo(fields[0],
                            parseInt(fields[5]),
                            parseFloat(fields[1]),
                            parseFloat(fields[2]),
                            parseFloat(fields[3]),
                            parseFloat(fields[4]),
                            fields[6].equalsIgnoreCase("TRUE") ? true : false,
                            fields[0]));
                }
            }
            csvReader.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
