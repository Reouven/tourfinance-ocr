package com.fractalite.ocr;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import java.io.ByteArrayOutputStream;
import java.util.Date;

public class OcrUtils {
    private static final String baseSeq = " !#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghijklmnopqrstuvwxyz{|}~";
    public static byte[] getPageBytes(PDDocument doc, int page) {
        return getPageBytes(doc.getPage(page - 1));
    }

    public static byte[] getPageBytes(PDPage page) {
        PDDocument newdoc = new PDDocument();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        newdoc.addPage(page);
        try {
            newdoc.save(outputStream);
        } catch (Exception e) {
            return new byte[0];
        }
        return outputStream.toByteArray();
    }

    public  static String generateUniqueFileName(String prefix) {
        String filename = "";
        long millis = System.currentTimeMillis();
        String datetime = new Date().toGMTString();
        datetime = datetime.replace(" ", "");
        datetime = datetime.replace(":", "");
        String rndchars = RandomStringUtils.randomAlphanumeric(16);
        filename = rndchars + "_" + datetime + "_" + millis;
        return prefix + filename;
    }

    public static float getPDPageHeight(PDPage page) {
        if (page.getTrimBox() != null)
            return (page.getTrimBox().getHeight());
        if (page.getCropBox() != null)
            return (page.getCropBox().getHeight());
        if (page.getBBox() != null)
            return (page.getBBox().getHeight());
        if (page.getBleedBox() != null)
            return (page.getBleedBox().getHeight());
        return (page.getMediaBox().getHeight());
    }
    public static float getPDPageWidth(PDPage page) {
        if (page.getTrimBox() != null)
            return (page.getTrimBox().getWidth());
        if (page.getCropBox() != null)
            return (page.getCropBox().getWidth());
        if (page.getBBox() != null)
            return (page.getBBox().getWidth());
        if (page.getBleedBox() != null)
            return (page.getBleedBox().getWidth());
        return (page.getMediaBox().getWidth());
    }

    public static String fromBase10ToBaseN(int inputValue, int N) {

        inputValue = Math.abs(inputValue);
        N = Math.min(N, 93);
        StringBuilder sb = new StringBuilder();
        do {
            sb.append(baseSeq.charAt(inputValue % N));
            inputValue /= N;
        } while (inputValue != 0);
        return sb.toString();
    }

}
