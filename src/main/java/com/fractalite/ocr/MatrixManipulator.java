package com.fractalite.ocr;

import technology.tabula.Ruling;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class MatrixManipulator {

    public static class MatrixCell {
        public Point2D point;
        public Ruling HRuling;
        public Ruling VRuling;

        public boolean enable = true;

        MatrixCell(Point2D p, Ruling HR, Ruling VR)
        {
            point = p;
            HRuling = HR;
            VRuling = VR;
        }
    }
    static MatrixCell getPointY(MatrixCell points[][], int x,int y, int tolerance) {
        for (int k = y; k < points.length; k++)
        {
            if (points[k][x] != null)
                return points[k][x];
            for (int i = 1; i <= tolerance && (x + i) < points[0].length; i++)
            {
                if (points[k][x + i] != null)
                    return points[k][x + i];
                if (x - i >= 0 && points[k][x - i] != null)
                    return points[k][x - i];
            }
        }
        return null;
    }

    static MatrixCell getPointX(MatrixCell points[][], int x,int y, int tolerance) {
        for (int k = x; k < points[0].length; k++)
        {
            if (points[y][k] != null)
                return points[y][k];
            for (int i = 1; i <= tolerance && (y + i) < points.length ; i++)
            {
                if (points[y + i][k] != null)
                    return points[y + i][k];
                if (y - i >= 0 && points[y - i][k]!= null)
                    return points[y - i][k];
            }
        }
        return null;
    }

    public static List<MatrixCell> getPointsX(MatrixCell points[][], int x, int y,int xe, int tolerance) {
        ArrayList<MatrixCell> cells = new ArrayList<MatrixCell>();

        for (int k = x + 1; k < xe; k++)
        {
            for (int i = 0; i <= tolerance && (y + i) < points.length ; i++)
            {
                if (points[y + i][k] != null)
                    cells.add(points[y + i][k]);
                else if (y - i >= 0 && points[y - i][k] != null)
                    cells.add(points[y - i][k]);
            }
        }
        return cells;
    }

    public static boolean isIntersected(List<MatrixCell> cells, Ruling line) {
        if (cells == null)
            return false;
        for (MatrixCell cell:cells) {
            if (cell.VRuling.intersectsLine(line))
                return true;
        }
        return false;
    }

    static void cleanArea(MatrixCell cells[][], int x,int y, int x1, int y1) {
        // disable the intersection on the edge of the rect
        for (int j = y ; j < y1 ; j++)
            if (cells[j][x-1] != null) {
                System.out.println("disable cell at " + (x-1) + "," + j);
                cells[j][x-1].enable = false;
            }

        // remove the intersection inside the rect
        int x0 = x;
        for (;y < y1; y++)
            for (x = x0 ; x < x1; x++)
                cells[y][x] = null;
    }
}
