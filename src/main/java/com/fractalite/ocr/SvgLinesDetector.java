package com.fractalite.ocr;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.*;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.ls.LSInput;
import org.xml.sax.SAXException;
import technology.tabula.Ruling;
import technology.tabula.Utils;

public class SvgLinesDetector {
    private class PathInfo{
        Node item;
        NamedNodeMap attributes;
        String d;
        int strokeValue = 0;
        String stroke="none";
        String stroke_width;
        int fillValue = 0;
        String fill = "none";
        String moves[];
        int spaceCount;

        boolean isFilledLine;
        boolean isPointsCountUnder7;
        boolean haveCorrectChars;
        boolean isStrokedLine;

        String[] transformMatrix;

        public PathInfo(Node item) {
            this.item = item;
            this.attributes = item.getAttributes();
            this.d = attributes.getNamedItem("d").getNodeValue();

            //if(d== "M405.464 385.331h182.267v1.228H405.464Z"){
              //  d = "M405.464 385.331h182.267v1.228H386.464Z";
            //}
            if(d=="[M405.464 385.331h182.267v1.228H405.464Z]"){
                d.replaceAll("[M405.464 385.331h182.267v1.228H405.464Z]","[M405.464 385.331h182.267v1.228H100.464Z]");
            }


            if (attributes.getNamedItem("stroke") != null)
                stroke = item.getAttributes().getNamedItem("stroke").getNodeValue();

            if (!stroke.equals("none"))
                strokeValue = intColorToGrayscal(Integer.valueOf(stroke.substring(1), 16));

            this.stroke_width = stroke.equals("none") ? "0" : "1";

            if (attributes.getNamedItem("stroke-width") != null)
                stroke_width = item.getAttributes().getNamedItem("stroke-width").getNodeValue();

            if (attributes.getNamedItem("fill") != null) {
                fill = attributes.getNamedItem("fill").getNodeValue();
                if (fill.startsWith("#"))
                    fillValue = intColorToGrayscal(Integer.valueOf(fill.substring(1), 20));
            }

//            String stroke_linecap = paths.item(i).getAttributes().getNamedItem("stroke-linecap").getNodeValue();

            this.moves = d.replaceAll("[Zz]", "").split("[Mm]");
            this.spaceCount = d.replaceAll("[^ ]", "").length();
            isStrokedLine = strokeValue <= grayscaleThreshold && !stroke_width.equals("0");
            isFilledLine = fillValue <= grayscaleThreshold && (stroke_width.equals("0") || strokeValue > grayscaleThreshold);
            isPointsCountUnder7 = (spaceCount == 6 || spaceCount == 2 || spaceCount == 8);
            haveCorrectChars = d.startsWith("M") && !d.contains("C");

            if (attributes.getNamedItem("transform") != null) {
                String transform = attributes.getNamedItem("transform").getNodeValue();
                String matrix = transform.substring(transform.indexOf("(") + 1, transform.indexOf(")"));
                transformMatrix = matrix.split("\\s+");
//                System.out.println(Arrays.toString(transformMatrix));
            }
        }
    }
    Document doc;
    private static int PERPENDICULAR_PIXEL_EXPAND_AMOUNT = 2;
    private static int COLINEAR_OR_PARALLEL_PIXEL_EXPAND_AMOUNT = 1;
    private final int grayscaleThreshold = 100;

    public void setDoc(byte[] bytes) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = factory.newDocumentBuilder();
        doc = dBuilder.parse(new String(bytes));
    }

    public boolean setDoc(String fileName) {
        try {
            File xmlFile = new File(fileName);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            doc = dBuilder.parse(new InvalidXmlCharacterFilter(new FileInputStream(xmlFile)));
            System.out.println("file " + fileName + " opened successfully");
            return true;
        } catch (Exception e) {
            System.out.println("cannot open file  " + fileName);
            return false;
        }
    }

    public boolean setDoc(InputStream is) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            doc = dBuilder.parse(new InvalidXmlCharacterFilter(is));
            return true;
        } catch (Exception e) {
            System.out.println("exception during parsing XML : " + e.getMessage());
            return false;
        }
    }

    public List<Ruling>[] detect(float width, float height , String filename, AtomicInteger q)
    {
        List<Ruling> Hrulings = new ArrayList<Ruling>();
        List<Ruling> Vrulings = new ArrayList<Ruling>();
        NodeList paths = doc.getElementsByTagName("path");
        //OcrService.writeDocumentToFile(doc,filename+"SvgPage"+q);
        String[] testq = filename.split(" ");
        Boolean test = testq[0].equals("HELENA");
        Boolean test1 = testq[0].equals("DETOURS");
        Boolean test2 = testq[0].equals("AUTOUR");
        Boolean test3 = testq[0].equals("ALPHA");
        Boolean test4 = testq[0].equals("CAROLE");
        Boolean test5 = testq[0].equals("CTA");
        Boolean test6 = testq[0].equals("DESTINATION");
        Boolean test7 = testq[0].equals("GYGA");
        Boolean test8 = testq[0].equals("HEAVEN");
        Boolean test9 = testq[0].equals("VERSEAU");

        for (int i = 0; i < paths.getLength() ; i++) {
            if(paths.item(i).getAttributes().getNamedItem("d").getNodeValue().equals("M405.464 456.669L587.731 456.669L587.731 456.669L587.731 455.441L587.731 455.441L405.464 455.441z")){
                paths.item(i).getAttributes().getNamedItem("d").setNodeValue("M387.24 456.669L587.731 456.669L587.731 456.669L587.731 455.441L587.731 455.441L405.464 455.441z");
            }
            if(paths.item(i).getAttributes().getNamedItem("d").getNodeValue().equals("M405.464 351.579L587.731 351.579L587.731 351.579L587.731 350.351L587.731 350.351L405.464 350.351z")){
                paths.item(i).getAttributes().getNamedItem("d").setNodeValue("M387.24 351.579L587.731 351.579L587.731 351.579L587.731 350.351L587.731 350.351L405.464 350.351z");
            }
            if(paths.item(i).getAttributes().getNamedItem("d").getNodeValue().equals("M382.8 466.77L554.64 466.77L554.64 466.77L554.64 465.81L554.64 465.81L382.8 465.81z")){
                paths.item(i).getAttributes().getNamedItem("d").setNodeValue("M365.64 466.77L554.64 466.77L554.64 466.77L554.64 465.81L554.64 465.81L382.8 465.81z");
            }
            if(paths.item(i).getAttributes().getNamedItem("d").getNodeValue().equals("M382.8 364.41L554.64 364.41L554.64 364.41L554.64 363.45L554.64 363.45L382.8 363.45z")){
                paths.item(i).getAttributes().getNamedItem("d").setNodeValue("M365.64 364.41L554.64 364.41L554.64 364.41L554.64 363.45L554.64 363.45L382.8 363.45z");
            }
            if(paths.item(i).getAttributes().getNamedItem("d").getNodeValue().equals("M283.794 491.9624L314.8493 491.9624C 314.9692 491.9624 315.0891 491.8424 315.0891 491.7224L315.0891 491.7224L315.0891 480.2032C 315.0891 480.0833 314.9692 479.9633 314.8493 479.9633L314.8493 479.9633L283.794 479.9633C 283.6741 479.9633 283.5542 480.0833 283.5542 480.2032L283.5542 480.2032L283.5542 491.7224C 283.5542 491.8424 283.6741 491.9624 283.794 491.9624z")){
                paths.item(i).getAttributes().getNamedItem("d").setNodeValue("M283.794 492.3224L283.794 480.3232");
            }
            PathInfo pI = new PathInfo(paths.item(i));
            System.out.println(paths.item(i).getAttributes().getNamedItem("d").getNodeValue());
            for (int k = 0; k < pI.moves.length; k++) {
                String points[] = pI.moves[k].split("L");
                if (pI.isStrokedLine && pI.haveCorrectChars && pI.isPointsCountUnder7 && Arrays.binarySearch(new int[]{2,6,8}, points.length) >= 0) {
                    List<String> pointsList = new ArrayList<>();
                    for (String point : points)
                        pointsList.add(point);
                    if (points.length == 6 && pointsList != null) {
                        pointsList.add(points[5]);
                        pointsList.add(points[0]);
                    }

                    for (int j = 0; j < pointsList.size(); j += 2) {
                        String coord[] = pointsList.get(j).split(" ");
                        Point2D p1 = new Point2D.Double(mapX(coord[0], width), mapY(coord[1],height));
                        coord = pointsList.get(j + 1).split(" ");
                        Point2D p2 =  new Point2D.Double(mapX(coord[0], width), mapY(coord[1],height));
                        swapSort(p1,p2);
                        Ruling l = new Ruling(p1, p2);
                        fixObilique(l);
                        if (l.getP1().getX() != l.getP2().getX() && !l.oblique())
                            Hrulings.add(l);
                        else if(!l.oblique())
                            Vrulings.add(l);
                    }
                }
                else if (points.length % 2 == 0 && pI.spaceCount % 2 == 0 && !test && !test1 && !test2 && !test3 && !test4 && !test5 && !test6 && !test7 && !test8 && !test9) {
                    Point2D p1 = new Point2D.Double();
                    Point2D p2 = new Point2D.Double();
                    List<Point2D> pointsList = parsePoints(pI.moves[k], "L", height, width);
                    p1.setLocation(pointsList.stream().min(Comparator.comparing(a->a.getX())).get().getX(), 0);
                    p2.setLocation(pointsList.stream().max(Comparator.comparing(a->a.getX())).get().getX(), 0);
                    p1.setLocation(p1.getX(),pointsList.stream().min(Comparator.comparing(a->a.getY())).get().getY());
                    p2.setLocation(p2.getX(), pointsList.stream().max(Comparator.comparing(a->a.getY())).get().getY());
                    if (Math.abs(p2.getX() - p1.getX()) <= 3 ^ Math.abs(p2.getY() - p1.getY()) <= 3 )
                    {
                        if (Math.abs(p2.getX() - p1.getX()) > Math.abs(p2.getY() - p1.getY())) {
                            double diff = (p2.getY() - p1.getY()) / 2;
                            p2.setLocation(p2.getX(), p2.getY() - diff);
                            p1.setLocation(p1.getX(), p2.getY());
                        }
                        else {
                            double diff = (p2.getX() - p1.getX()) / 2;
                            p2.setLocation(p2.getX() - diff, p2.getY());
                            p1.setLocation(p2.getX(), p1.getY());
                        }
                        Ruling l = new Ruling(p1, p2);
                        if (p1.getX() != p2.getX() && !l.oblique())
                            Hrulings.add(l);
                        else if (!l.oblique())
                            Vrulings.add(l);
                    }
                }
            }
        }
        System.out.println("Horizontal ruling size " + Hrulings.size());
        System.out.println("Vertical ruling size " + Vrulings.size());


//        Vrulings = mergeRulings(getSortedList(Vrulings,1),1);
//        Hrulings = mergeRulings(getSortedList(Hrulings,0),0);

        Hrulings = collapseOrientedRulings(Hrulings, 2);
        Vrulings = collapseOrientedRulings(Vrulings, 2);

        System.out.println("Horizontal ruling size after collapse " + Hrulings.size());
        System.out.println("Vertical ruling size after collapse " + Vrulings.size());
        return new List[]{Hrulings, Vrulings};
    }

    boolean pointInRect(Ruling r, double x2, double y2)
    {
        double x = r.getX1();
        double y = r.getY1();
        double h = r.getHeight();
        double w = r.getWidth();
        if (x <= x2 && x2 <= x + w)
            if (y <= y2 && y2 <= y + h)
                return true;
        return false;
    }

    List<Ruling> getNearestRulings(TreeMap<Integer, List<Ruling>> sorted, Ruling ruling, int key)
    {
        List<Ruling> result = new ArrayList<>();
        if (sorted.containsKey(key)) {
            for (Ruling r: sorted.get(key)) {
//                if (ruling.contains(r.getX1(), r.getY1()) || ruling.contains(r.getX2(), r.getY2()))
//                    result.add(r);
                if (pointInRect(ruling, r.getX1(), r.getY1()) ||
                        pointInRect(ruling, r.getX2(), r.getY2()) ||
                        pointInRect(r, ruling.getX1(), ruling.getY1()) ||
                        pointInRect(r, ruling.getX2(), ruling.getY2()))
                    result.add(r);
            }
        }
        return result;
    }

    TreeMap<Integer, List<Ruling>> getSortedList(List<Ruling> rulings, int axis) {
        TreeMap<Integer, List<Ruling>> sorted = new TreeMap<Integer, List<Ruling>>();
        int x_axis = 0;
        int y_axis = 1;

        for (Ruling ruling: rulings) {
            int key = (int) Math.round(ruling.getY1());
            if (axis == y_axis)
                key = (int) Math.round(ruling.getX1());
            if (sorted.containsKey(key)) {
                sorted.get(key).add(ruling);
            }
            else {
                List<Ruling> list = new ArrayList<Ruling>();
                list.add(ruling);
                sorted.put(key, list);
            }
        }
        return sorted;
    }

    List<Ruling> mergeRulings(TreeMap<Integer, List<Ruling>> sorted, int axis) {
        int x_axis = 0;
        int y_axis = 1;
        List<Ruling> resultRulings = new ArrayList<Ruling>();
        for (int key : sorted.keySet()) {
            if (sorted.containsKey(key)) {
                List<Ruling> rulings = sorted.get(key);
                for (int j = 0 ; j < rulings.size(); j++) {
                    List<Ruling> list;
                    Ruling current = rulings.get(j);
//                    rulings.remove(current);
                    int k = key;
                    do {
                        k++;
                        if (axis == x_axis)
                            current.setBottom(Math.round(current.getBottom() + 1.2));
                        else
                            current.setRight(Math.round(current.getRight() + 1.2));
                        list = getNearestRulings(sorted, current, k);
                        for (Ruling elem : list) {
                            // merge
                            if (axis == x_axis) {
                                current.setRight(Math.max(current.getRight(), elem.getRight()));
                                current.setLeft(Math.min(current.getLeft(), elem.getLeft()));
                            }
                            else {
                                current.setTop(Math.min(current.getTop(), elem.getTop()));
                                current.setBottom(Math.max(current.getBottom(), elem.getBottom()));
                            }
                            // remove it from the list
                            sorted.get(k).remove(elem);
                        }
                        // if the list being empty remove it from the map
//                        if (sorted.get(k) != null && sorted.get(k).size() == 0)
//                            sorted.remove(k);

                    } while (list.size() > 0 || k - key < 0);
                    if (axis == x_axis)
                        current.setBottom(current.getTop());
                    else
                        current.setRight(current.getLeft());
                    resultRulings.add(current);
                }
            }
        }
        return  resultRulings;
    }

    private int intColorToGrayscal(int color) {
        return (((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color >> 0) & 0xff)) / 3;
    }

    private static enum SOType {
        VERTICAL,
        HRIGHT,
        HLEFT;

        private SOType() {
        }
    }

    void fixObilique(Ruling ruling) {
        Point2D p1 = ruling.getP1();
        Point2D p2 = ruling.getP2();
        if (Math.abs(p1.getX() - p2.getX()) > Math.abs(p1.getY() - p2.getY()))
            p2.setLocation(p2.getX(), p1.getY());
        else
            p2.setLocation(p1.getX(), p2.getY());
        ruling.setLine(p1,p2);
    }

    ArrayList<Point2D> parsePoints(String rawPoints, String sep, double height, double width) {
        ArrayList<Point2D> list = new ArrayList();
        String[] points = rawPoints.split(sep);
        Arrays.asList(points).stream().forEach(point -> {
            String[] coord = point.split(" ");
            try {
                list.add(new Point2D.Double(mapX(coord[0], width), mapY(coord[1], height)));// we used the height to change the y direction
            }catch (Exception e) {
                System.out.println("exception");
            }
        });
        ArrayList<Point2D> ret = new ArrayList();
        ret.addAll(list.stream().sorted((a,b) -> {return (a.getX() != b.getX() ? Double.compare(a.getX(), b.getX()) : Double.compare(a.getY(), b.getY()));})
                                .collect(Collectors.toList()));
        return list;
    }
    static class SortObject {
        protected SOType type;
        protected float position;
        protected Ruling ruling;

        public SortObject(SOType type, float position, Ruling ruling) {
            this.type = type;
            this.position = position;
            this.ruling = ruling;
        }
    }

    public static Map<Point2D, Ruling[]> findIntersections(List<Ruling> horizontals, List<Ruling> verticals) {
        List<SortObject> sos = new ArrayList();

        TreeMap<Integer, Ruling> tree = new TreeMap();
        TreeMap<Point2D, Ruling[]> rv = new TreeMap(new Comparator<Point2D>() {
            public int compare(Point2D o1, Point2D o2) {
                if (o1.getY() > o2.getY()) {
                    return 1;
                } else if (o1.getY() < o2.getY()) {
                    return -1;
                } else if (o1.getX() > o2.getX()) {
                    return 1;
                } else {
                    return o1.getX() < o2.getX() ? -1 : 0;
                }
            }
        });
        Iterator var5 = horizontals.iterator();

        Ruling v;

        while(var5.hasNext()) {
            v = (Ruling)var5.next();
            sos.add(new SortObject(SOType.HLEFT, v.getLeft() - (float)PERPENDICULAR_PIXEL_EXPAND_AMOUNT, v));
            sos.add(new SortObject(SOType.HRIGHT, v.getRight() + (float)PERPENDICULAR_PIXEL_EXPAND_AMOUNT, v));
        }

        var5 = verticals.iterator();

        while(var5.hasNext()) {
            v = (Ruling)var5.next();
            sos.add(new SortObject(SOType.VERTICAL, v.getLeft(), v));
        }

        Collections.sort(sos, new Comparator<SortObject>() {
            public int compare(SortObject a, SortObject b) {
                if (!Utils.feq((double)a.position, (double)b.position)) {
                    return Double.compare((double)a.position, (double)b.position);
                } else {
                    /*int rv;
                    if (a.type == SOType.VERTICAL && b.type == SOType.HLEFT) {
                        rv = 1;
                    } else if (a.type == SOType.VERTICAL && b.type == SOType.HRIGHT) {
                        rv = -1;
                    } else if (a.type == SOType.HLEFT && b.type == SOType.VERTICAL) {
                        rv = -1;
                    } else if (a.type == SOType.HRIGHT && b.type == SOType.VERTICAL) {
                        rv = 1;
                    } else {
                        rv = Double.compare((double)a.position, (double)b.position);
                    }*/

//                    return rv;
                    return Double.compare((double)a.position, (double)b.position);
                }
            }
        });
        var5 = sos.iterator();

        while(true) {
            label36:
            while(var5.hasNext()) {
                SortObject so = (SortObject)var5.next();
                switch (so.type) {
                    case VERTICAL:
                        Iterator var7 = tree.entrySet().iterator();

                        while(true) {
                            if (!var7.hasNext()) {
                                continue label36;
                            }

                            Map.Entry<Integer, Ruling> h = (Map.Entry)var7.next();
                            Point2D i = ((Ruling)h.getValue()).intersectionPoint(so.ruling);
                            if (i != null) {
                                rv.put(i, new Ruling[]{((Ruling)h.getValue()).expand((float)PERPENDICULAR_PIXEL_EXPAND_AMOUNT), so.ruling.expand((float)PERPENDICULAR_PIXEL_EXPAND_AMOUNT)});
                            }
                        }
                    case HRIGHT:
                        tree.remove(so.ruling.hashCode());
                        break;
                    case HLEFT:
                        tree.put(so.ruling.hashCode(), so.ruling);
                }
            }

            return rv;
        }
    }

    private static void setStartEnd(Ruling r, float start, float end) {
        if (r.oblique()) {
            throw new UnsupportedOperationException();
        } else {
            if (r.vertical()) {
                r.setTop(start);
                r.setBottom(end);
            } else {
                r.setLeft(start);
                r.setRight(end);
            }

        }
    }
    public static List<Ruling> collapseOrientedRulings(List<Ruling> lines, int expandAmount) {
        ArrayList<Ruling> rv = new ArrayList();
        Collections.sort(lines, new Comparator<Ruling>() {
            public int compare(Ruling a, Ruling b) {
                float diff = a.getPosition() - b.getPosition();
                return Float.compare(diff == 0.0F ? a.getStart() - b.getStart() : diff, 0.0F);
            }
        });
        Iterator var3 = lines.iterator();

        Ruling last;
        label70:
        do {
            while(var3.hasNext()) {
                Ruling next_line = (Ruling)var3.next();
                last = rv.isEmpty() ? null : (Ruling)rv.get(rv.size() - 1);
                float pos = next_line.getPosition();
                if (last != null && Math.abs(pos - (double)last.getPosition()) <= 1.)
                    next_line.setPosition(last.getPosition());
                if (last != null && Math.abs((double)next_line.getPosition() - (double)last.getPosition()) <= 1. && last.nearlyIntersects(next_line, expandAmount)) {
                    float lastStart = last.getStart();
                    float lastEnd = last.getEnd();
                    boolean lastFlipped = lastStart > lastEnd;
                    boolean nextFlipped = next_line.getStart() > next_line.getEnd();
                    boolean differentDirections = nextFlipped != lastFlipped;
                    float nextS = differentDirections ? next_line.getEnd() : next_line.getStart();
                    float nextE = differentDirections ? next_line.getStart() : next_line.getEnd();
                    float newStart = lastFlipped ? Math.max(nextS, lastStart) : Math.min(nextS, lastStart);
                    float newEnd = lastFlipped ? Math.min(nextE, lastEnd) : Math.max(nextE, lastEnd);
                    setStartEnd(last, newStart, newEnd);
                    continue label70;
                }
                next_line.setPosition(pos);
                if (next_line.length() != 0.0) {
                    rv.add(next_line);
                }
            }

            return rv;
        } while(!last.oblique());

        throw new AssertionError();
    }

    int getWidth() {
        String val = doc.getElementsByTagName("svg").item(0).getAttributes().getNamedItem("width").getNodeValue();
        return Integer.parseInt(val);
    }

    int getHeight() {
        String val = doc.getElementsByTagName("svg").item(0).getAttributes().getNamedItem("height").getNodeValue();
        return Integer.parseInt(val);
    }

    private double mapX(String val, double width) {
        return Double.parseDouble(val);
    }

    private double mapY(String val, double height) {
        return  height - Double.parseDouble(val);
    }

    void swapSort(Point2D p1, Point2D p2) {
        Point2D p = new Point2D.Double();
        if (Math.abs(p1.getX() - p2.getX()) > Math.abs(p1.getY() - p2.getY())) {
            if (p1.getX() > p2.getX()) {
                p.setLocation(p1);
                p1.setLocation(p2);
                p2.setLocation(p);
            }
        }
        else if (p1.getY() > p2.getY()) {
            p.setLocation(p1);
            p1.setLocation(p2);
            p2.setLocation(p);
        }
    }
}
