echo " --------------------------------------------------------------------------------- "
echo " --------------------------------------------------------------------------------- "
echo " ------------------------------------ build the jar ------------------------------ "

mvn clean install -DskipTests

echo " ------------------------------------ deploy ocr ----------------------------------- "

scp ./target/ocr-0.0.1-SNAPSHOT.jar ubuntu@63.32.125.159:/home/ubuntu/ocr
scp ./start.sh ubuntu@63.32.125.159:/home/ubuntu/ocr
scp ./src/main/resources/application.properties ubuntu@63.32.125.159:/home/ubuntu/ocr
#scp -r  ./generated_cords ubuntu@63.32.125.159:/home/ubuntu/


echo " ------------------------------------ restart ocr ---------------------------------- "

ssh ubuntu@63.32.125.159 "pm2 restart ocr;"
